Ubiquity
========

The prototype of Ubiquity.

Development
-----------

Created using FlashDevelop. Written in ActionScript3 using the FlashPunk framework. FlashPunk source can be found [here](https://github.com/Draknek/FlashPunk).
Levels are created using OGMO EDITOR which can be found [here](http://www.ogmoeditor.com/).

Features
--------

Features implemented:

* Teleporting
* Shooting - NOT USED
* World Switching
* Doors and Switches
* Hostile Aliens
* Patrolling Aliens
* Tether
* Moveable Objects
* Power Cells
* Turrets

Features still to implement:

* None at the moment! 

User Guide
----------

### Controls
####Menu:

* UP and DOWN arrow keys to select level
* SPACE to enter level

####Game:

* WASD to move player
* LEFT MOUSE click to fire the tether
* MOVE MOUSE to move tethered object.
* G to swap worlds
* T to swap places with tethered object
* V to teleport only tethered object

### Goal
To beat the level, pick up the power cells from the alien world and move them into the power cell sockets in the normal world. Then reach the finish line.
You can take 3 hits before your ship is destroyed.

Credits
-------

*Programming*:
Adam Docherty and Michael Cameron

*Art*:
Callum MacDougall

*Design*:
Thomas Garnerone

*Music and SFX*: Mike Stuart and Andrew Wood

####The full Velocity Raptors team:

* Michael Grooves (Producer)
* Thomas Garnerone (Designer)
* Callum MacDougall (Lead Artist)
* Byran Carr (Artist)
* Ellen Brown (Artist)
* Caitlin Goodale (Artist)
* Michael Cameron (Lead Programmer)
* Adam Docherty (Programmer)
* Mark O'Donnell (Programmer)
