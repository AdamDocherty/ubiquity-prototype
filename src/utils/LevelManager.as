package utils 
{
	import entities.AntiTeleportField;
	import entities.EnemySentry;
	import entities.PlayerShip;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import worlds.GameWorld;
	import worlds.MenuWorld;
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class LevelManager 
	{
		// The world the player is currently in. If 0 then no world is currently in use.
		protected var _currentWorld:int;
		
		protected var _music:Sfx;
		protected var _sfxDoor:Sfx;
		protected var _sfxWorldSwitch:Sfx;
		
		protected var _levels:Vector.<Level>;
		
		protected var _levelNumber:uint;
		
		public var _numberPowerCellSockets:uint;

		public function LevelManager() 
		{			
			_music = new Sfx(A.MUSIC_GAME_DEFAULT_BGM);
			_sfxDoor = new Sfx(A.SFX_DOOR);
			_sfxWorldSwitch = new Sfx(A.SFX_WORLD_SWAP);
			_currentWorld = -1;
			
			_levels = new Vector.<Level>;
		}
		
		public function setLevel(index:int):void
		{
			_numberPowerCellSockets = 0;
			
			// Since the levels are numbered from 1 rather than from 0. The level 
			// number is one higher than the index.
			_levelNumber = ++index;
			
			// Gets the of the level as it appears in the A class
			// This assumes all levels follow the naming structure "LEVEL_xx_A/B".
			var strA:String = "LEVEL_";
			if (_levelNumber < 10) { strA += "0"; }
			strA += _levelNumber.toString();
			
			var strB:String = strA;
			strA += "_A";
			strB += "_B";
			
			// Creates the two worlds
			_levels.push(new Level(0, new GameWorld(this, A[strA], index)));
			_levels.push(new Level(1, new GameWorld(this, A[strB], index)));
		}
		
		public function resetLevel():void
		{
			_numberPowerCellSockets = 0;
			
			_levels.pop()
			_levels.pop();
			
			var strA:String = "LEVEL_";
			if (_levelNumber < 10) { strA += "0"; }
			strA += _levelNumber.toString();
			
			var strB:String = strA;
			strA += "_A";
			strB += "_B";
			
			// Creates the two worlds
			_levels.push(new Level(0, new GameWorld(this, A[strA], _levelNumber)));
			_levels.push(new Level(1, new GameWorld(this, A[strB], _levelNumber)));
			
			startLevel();
		}
		
		public function startLevel():void
		{
			_music.loop();
			_currentWorld = 0;
			FP.world = _levels[_currentWorld].world;
		}
		
		public function endLevel():void
		{
			_music.stop();
			
			// Go to the menu with the next level selected
			FP.world = new MenuWorld(_levelNumber + 1);
		}
		
		public function swapObject(e:Entity):void
		{
			if (e.collideTypes(AntiTeleportField.TYPE, e.x, e.y)) return;
			
			switch(_currentWorld)
			{
				// TODO: Add collision detection when swapping worlds so objects don't get stuck in walls
				case 0:
					_levels[0].world.remove(e);
					_levels[1].world.add(e);
					if (e as EnemySentry)
						EnemySentry(e).player = _levels[1].world.player;
					break;
				case 1:
					_levels[1].world.remove(e);
					_levels[0].world.add(e);
					if (e as EnemySentry)
						EnemySentry(e).player = _levels[0].world.player;
					break;
				default:
					trace("currentWorld set at an incorrect value");
			}
		}
		
		public function swapWorld():void
		{
			var player:PlayerShip = _levels[_currentWorld].world.player;
			
			if (player.collideTypes(AntiTeleportField.TYPE, player.x, player.y))
				return;
			if (player.tetheredObject != null) {
				if (player.tetheredObject.collideTypes(AntiTeleportField.TYPE, 
					player.tetheredObject.x, player.tetheredObject.y)) return;
			}
			
			var result:Boolean = false;
			
			switch(_currentWorld)
			{
				case 0:
					result = _levels[1].enter(_levels[_currentWorld]);
					if (!result) break;
					
					FP.world = _levels[1].world;
					_currentWorld = 1;
					break;
				case 1:
					result = _levels[0].enter(_levels[_currentWorld]);
					if (!result) break;
					
					FP.world = _levels[0].world;
					_currentWorld = 0;
					break;
				default:
					trace("currentWorld set at an incorrect value");
			}
			
			if (result)
				_sfxWorldSwitch.play();
		}
		
		public function switchActivated(switchLabel:String):void
		{
			var doorsOpened:Boolean = false;
			
			for each (var l:Level in _levels)
			{
				if (l.world.unlockDoors(switchLabel))
					doorsOpened = true;
			}
			
			if (doorsOpened)
				_sfxDoor.play();
		}
	}
}
import entities.TetherableBase;
import worlds.GameWorld;

class Level
{
	private var _index:int;
	private var _world:GameWorld;
	
	public function get index():int { return _index; }
	
	public function get world():GameWorld { return _world; }
	
	public function Level(index:int, world:GameWorld)
	{
		_index = index;
		_world = world;
	}
	
	/**
	 * 
	 * @param	prevLevel	the world we are swapping from
	 * @return	true if world swap was successful, false otherwise
	 */
	public function enter(prevLevel:Level):Boolean
	{
		prevLevel.exit();
		
		_world.player.x = prevLevel.world.player.x;
		_world.player.y = prevLevel.world.player.y;
		_world.player.health = prevLevel.world.player.health;
		
		if (_world.map.collideWith(_world.player, _world.map.x, _world.map.y))
		{
			return false;
		}
		
		_world.camera.y = prevLevel.world.camera.y;
				
		if (prevLevel.world.player.tetheredObject != null)
		{
			var to:TetherableBase = prevLevel.world.player.tetheredObject;
			
			prevLevel.world.player.deactivateTether();
			
			prevLevel.world.remove(to);
			_world.add(to);
			to.tetherTo(_world.player);
			_world.player.activateTether();
		}
		
		return true;
	}
	
	public function exit():void
	{
		
	}
}