package worlds 
{
	import entities.LevelMenuItem;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.misc.MultiVarTween;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	import utils.LevelManager;
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class MenuWorld extends World
	{
		protected var _music:Sfx;
		
		protected var _levelManager:LevelManager;
		
		protected var _levelList:Vector.<LevelMenuItem>;
		protected var _currentIndex:uint;
		protected var _menuTween:MultiVarTween;
		protected var _cx:Number;
		protected var _cy:Number;
		
		public function get cx():Number { return _cx; }
		public function set cx(val:Number):void { _cx = val; }
		
		public function get cy():Number { return _cy; }
		public function set cy(val:Number):void { _cy = val; }
		
		public function MenuWorld(index:int = 0) 
		{
			_levelManager = new LevelManager;
			_music = new Sfx(A.MUSIC_MENU_BGM);
			
			_levelList = new Vector.<LevelMenuItem>();
			_menuTween = new MultiVarTween();
			addTween(_menuTween, false);
			var i:uint = 1;
			
			// Iterate through our Assets, grabbing all the levels
			while (A[getLevelString(i)])
			{
				var yPosition:Number = (i - 1) * -40;
				_levelList.push(new LevelMenuItem(0, yPosition, A[getLevelString(i)]));
				++i;
			}
			
			// Positioning for the menu.
			_cx = 200;
			_cy = 0;
			_currentIndex = (index >= _levelList.length) ? index = _levelList.length - 1 : index;
			
		}
		
		override public function begin():void 
		{
			_music.loop();
			
			for each (var l:LevelMenuItem in _levelList)
			{
				add(l);
			}
			
			selectItem(_currentIndex);
			
			super.begin();
		}
		
		override public function end():void 
		{
			_music.stop();
			
			super.end();
		}
		
		override public function update():void 
		{
			var i:int = 0;
			
			centerScreenAt(_cx, _cy);
			
			if (Input.pressed(Key.SPACE))
			{
				load(_currentIndex);
			}
			else
			{
				if (Input.pressed(Key.UP)) ++i;
				if (Input.pressed(Key.DOWN)) --i;
				
				selectItem(_currentIndex + i);
			}
			
			super.update();
		}
		
		/**
		 * Changes the currently selected item
		 * @param	index	The index of the new item to be selected
		 */
		protected function selectItem(index:int):void
		{
			// Prevent out-of-bounds level selection.
			if (index >= _levelList.length) { index = _levelList.length - 1; }
			else if (index < 0) { index = 0; }

			// Update the screen rotation and position.
			_menuTween.tween(this, { cx:_levelList[index].x, cy:_levelList[index].y}, 0.5, Ease.quintOut);
			_menuTween.start();

			// Deselect the previous level and select the current level.
			_levelList[_currentIndex].selected = false;
			_levelList[index].selected = true;

			// Store our new index.
			_currentIndex = index;
		}
		
		/**
		 * Generate the string name of a level based on a provided index.
		 * @param	index The index of the level to generate a string name.
		 * @return The string name of a level.
		 */
		protected function getLevelString(index:int):String
		{
			// This assumes all levels follow the naming structure "LEVEL_xx".
			var s:String = "LEVEL_";
			if (index < 10) { s += "0"; }
			s += index.toString();
			s += "_A";
			return s;
		}

		/**
		 * Creates a GameWorld based on a provided level index.
		 * @param	index The index of the world to load.
		 */
		protected function load(index:int):void
		{
			_levelManager.setLevel(index);
			_levelManager.startLevel();
			//FP.world = new GameWorld(_levelList[index].data, index);
		}
		
		protected function centerScreenAt(cx:int, cy:int):void
		{
			camera.x = cx - FP.screen.width * 0.5;
			camera.y = cy - FP.screen.height * 0.5;
		}
		
	}

}