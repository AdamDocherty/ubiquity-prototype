package worlds 
{
	import adobe.utils.ProductManager;
	import entities.AntiTeleportField;
	import entities.Door;
	import entities.FinishArea;
	import entities.ui.HealthText;
	import entities.Path;
	import entities.PlayerShip;
	import entities.Pod;
	import entities.PowerCell;
	import entities.ui.PowerCellsCollectedText;
	import entities.PowerCellSocket;
	import entities.Switch;
	import entities.TeleportCursor;
	import entities.EnemySentry;
	import entities.Turret;
	import entities.DimensionalTrap;
	import enums.EnemyType;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.masks.Grid;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.World;
	import utils.LevelManager;
	
	/**
	 * The main game screen.
	 * @author Adam Docherty
	 */
	public class GameWorld extends World 
	{
		public static const SCROLL_SPEED_FAST:int = 80;
		public static const SCROLL_SPEED_SLOW:int = 10;
		public static var s_scrollSpeed:int;
		
		protected var _levelManager:LevelManager;
		
		protected var _map:Entity;
		protected var _player:PlayerShip;
		//protected var _teleportCursor:TeleportCursor;
		
		protected var _levelNumber:int;
		protected var _mapGrid:Grid;
		protected var _mapImage:Image;
		protected var _mapData:Class;
		protected var _levelWidth:uint;
		protected var _levelHeight:uint;
		
		protected var _numberPods:uint;
		protected var _numberSockets:uint;
		protected var _powerCellsCollectedText:PowerCellsCollectedText;
		
		protected var _gameState:uint;
		protected const STATE_RUNNING:uint = 0;
		protected const STATE_SUCCESS:uint = 1;
		protected const STATE_FAILURE:uint = 2;
		
		public function get levelManager():LevelManager { return _levelManager; }
		
		public function get player():PlayerShip { return _player; }
		public function get map():Entity { return _map; }
		
		/**
		 * @param	mapData	Map data in xml or oel formal. If no map data is provided a blank debug map will be made.
		 */
		public function GameWorld(levelManager:LevelManager, mapData:Class = null, index:int = 0) 
		{			
			_levelManager = levelManager;
			_levelNumber = index;
			_mapData = mapData;
			
			if (_mapData) 
			{
				loadMap(_mapData);
			}
			else
			{
				// Create a debug map.
				_mapGrid = new Grid(1280, 1280, C.LEVEL_GRID_SIZE, C.LEVEL_GRID_SIZE, 0, 0);
				_mapGrid.usePositions = true;
				_mapGrid.setRect(0, 0, 1280, 1280, true);
				_mapGrid.setRect(C.LEVEL_GRID_SIZE, C.LEVEL_GRID_SIZE, 1216, 1216, false);
				_player = new PlayerShip(360, 460);
				_numberPods = 0;
			}
			
			// Prepare the particle effects for the player's teleport trail
			addGraphic(_player.teleportTrail);
			addGraphic(_player.tetherBeam);
			add(new HealthText(_player));
			_powerCellsCollectedText = new PowerCellsCollectedText(_numberSockets);
			add(_powerCellsCollectedText);
			
			s_scrollSpeed = SCROLL_SPEED_SLOW;
			camera.y = _player.y - FP.screen.height * 0.7;
			
			_map = new Entity(0, 0, _mapImage, _mapGrid);
			_map.layer = 10;
			_map.type = "wall";
			
			//_teleportCursor = new TeleportCursor(_player);
		}
		
		override public function begin():void 
		{			
			add(_player);
			add(_map);
			//add(_teleportCursor);
						
			super.begin();
		}
		
		override public function end():void 
		{
			remove(_player);
			remove(_map);
			//remove(_teleportCursor);
			
			super.end();
		}
		
		override public function update():void 
		{						
			super.update();
			
			// Let the user quit at any time
			if (Input.pressed(Key.ESCAPE)) _gameState = STATE_FAILURE;
			
			switch (_gameState)
			{
				case STATE_RUNNING:
					updateRunning();
					break;
				case STATE_SUCCESS:
					updateSuccess();
					break;
				case STATE_FAILURE:
					updateFailure();
					break;
				default:
					trace("ERROR: No State");
			}
		}
		
		public function unlockDoors(label:String):Boolean
		{
			var door:Door;
			var doorVector:Vector.<Door> = new Vector.<Door>;
			
			getType(Door.DOOR_CLOSED_TYPE, doorVector);
			
			if (doorVector.length == 0)
			{
				return false; // No doors opened
			}
			
			for each (door in doorVector)
			{
				if (door.label == label)
				{
					door.open();
				}
			}
			
			return true; // Doors Opened
		}
		
		protected function updateRunning():void
		{
			// Test if the player uses the switch worlds button
			if (Input.pressed(Key.G))
			{
				_levelManager.swapWorld();
			}
			
			if (Input.pressed(Key.V))
			{
				if (_player.tetheredObject != null) {
					_levelManager.swapObject(_player.tetheredObject);
					_player.deactivateTether();
				}
			}
			
			toggleScrollSpeed();
			
			handleFinishCollision();
			
			// Center the camera x position to the player
			camera.x = _player.x - FP.screen.width * 0.5;
			// locks the x position to the edges of the level so the player cannot teleport outside
			if (camera.x < 0) camera.x = 0;
			if (camera.x + FP.screen.width > _levelWidth) camera.x = _levelWidth - FP.screen.width;
			// scrolls the camera vertically
			camera.y = _player.y - FP.screen.height * 0.5;
			// locks the y position of the camera to the edges of the level
			if (camera.y < 0) camera.y = 0;
			if (camera.y + FP.screen.height > _levelHeight) camera.y = _levelHeight - FP.screen.height;
			
			if (_player.checkIsDead())
			{
				_gameState = STATE_FAILURE;
			}
		}
		
		protected function updateSuccess():void
		{
			_levelManager.endLevel();
		}
		
		protected function updateFailure():void
		{
			_levelManager.resetLevel();
		}
		
		protected function handleFinishCollision():void
		{
			var finish:FinishArea = FinishArea(_player.collide("finish", _player.x, _player.y));
			if (finish != null)
			{
				if (levelManager._numberPowerCellSockets == 0)
					_gameState = STATE_SUCCESS;
			}
		}
		
		protected function toggleScrollSpeed():void
		{
			// Stop the scrolling at the top of the screen
			if (camera.y < 0) 
			{
				s_scrollSpeed = 0;
				return;
			}		
			
			if (Input.check(Key.R))
			{
				s_scrollSpeed = SCROLL_SPEED_FAST;
			}
			else
			{
				s_scrollSpeed = SCROLL_SPEED_SLOW;
			}
		}
		
		protected function loadMap(mapData:Class):void
		{
			var mapXML:XML = FP.getXML(mapData);
			var node:XML;
			var pathNode:XML;
			
			_levelWidth = mapXML.@width;
			_levelHeight = mapXML.@height;
			
			// Create map Grid
			_mapGrid = new Grid(uint(mapXML.@width), uint(mapXML.@height), C.LEVEL_GRID_SIZE, C.LEVEL_GRID_SIZE, 0, 0);
			_mapGrid.loadFromString(String(mapXML.Grid), "", "\n");
			
			// Create a map image using the grid data
			var mi:Image = new Image(_mapGrid.data);
			mi.scale = C.LEVEL_GRID_SIZE;

			// Set the map image.
			_mapImage = mi;
			
			if (mapXML.@AlternateWorld == "True")
			{
				_mapImage.color = 0x964B00;
			}
			
			// Create the player at the player start
			_player = new PlayerShip(int(mapXML.Entities.Player_Start.@x),
				int(mapXML.Entities.Player_Start.@y));
				
			// Create the pods and count the total number of pods in the level
			_numberPods = 0;
			for each (node in mapXML.Entities.Pod)
			{
				add(new Pod(int(node.@x), int(node.@y)));
				++_numberPods;
			}
			
			// Create the finish areas
			for each (node in mapXML.Entities.Finish)
			{
				add(new FinishArea(int(node.@x), int(node.@y), int(node.@width), int(node.@height)));
			}
			
			// Add the doors
			for each (node in mapXML.Entities.Door)
			{
				// Check if the door label has been set
				if (String(node.@label) == "ERROR_DOOR")
				{
					trace("Warning: Door at ", int(node.@x), int(node.@y), "has no label");
				}
				add(new Door(int(node.@x), int(node.@y), String(node.@label)));
			}
			
			// Add the door switches
			for each (node in mapXML.Entities.DoorSwitch)
			{
				// check if the switch label has been set
				if (String(node.@label) == "ERROR_SWITCH")
				{
					trace("Warning: Door Switch at ", int(node.@x), int(node.@y), "has no label");
				}
				add(new Switch(int(node.@x), int(node.@y), String(node.@label)));
			}	
			for each (node in mapXML.Entities.PowerCell)
			{
				add(new PowerCell(int(node.@x), int(node.@y)));
			}
			
			for each (node in mapXML.Entities.PowerCellSocket)
			{
				add(new PowerCellSocket(int(node.@x), int(node.@y)));
				_levelManager._numberPowerCellSockets++;
			}
			_numberSockets = _levelManager._numberPowerCellSockets;
			// Add the small enemies.
			for each (node in mapXML.Entities.EnemySentry)
			{
				var path:Path = new Path;
				path.addNode(int(node.@x), int(node.@y));
				for each (pathNode in node.node)
				{
					path.addNode(int(pathNode.@x), int(pathNode.@y));
				}
				if (path.length <= 1) path = null;
				add(new EnemySentry(int(node.@x), int(node.@y), (Number(node.@angle))*(180/Math.PI), _player, EnemyType.SMALL, path));
			}
			
			// Add the slender enemies
			for each (node in mapXML.Entities.EnemySlender)
			{
				var spath:Path = new Path;
				spath.addNode(int(node.@x), int(node.@y));
				for each (pathNode in node.node)
				{
					spath.addNode(int(pathNode.@x), int(pathNode.@y));
				}
				if (spath.length <= 1) spath = null;
				add(new EnemySentry(int(node.@x), int(node.@y), (Number(node.@angle)) * (180 / Math.PI), _player, EnemyType.SLENDER, spath));
			}
			
			// Add the big enemies
			for each (node in mapXML.Entities.EnemyBig)
			{
				add(new EnemySentry(int(node.@x), int(node.@y), (Number(node.@angle)) * (180 / Math.PI), _player, EnemyType.LARGE, null));
			}
			
			// Add turrets.
			for each (node in mapXML.Entities.Turret)
			{
				add(new Turret(int(node.@x), int(node.@y), (Number(node.@angle))*(180/Math.PI)));
			}
			
			// Create the anti-teleport fields
			for each (node in mapXML.Entities.AntiTeleportField)
			{
				add(new AntiTeleportField(int(node.@x), int(node.@y), int(node.@width), int(node.@height)));
			}
			
			// Create the Dimesnional Swap Traps.
			for each (node in mapXML.Entities.DimensionalTrap)
			{
				add(new DimensionalTrap(int(node.@x), int(node.@y)));
			}
			
			// Draw a picture for the boss
			for each (node in mapXML.Entities.Boss)
			{
				var bossImage:Image = new Image(A.IMAGE_BOSS);
				add(new Entity(node.@x, node.@y, bossImage));
			}
		}
	}
}