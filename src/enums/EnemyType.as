package enums 
{
	/**
	 * Enumeration for enemy types
	 * @author Adam Docherty
	 */
	public final class EnemyType
	{
		
		public static const SMALL:int = 1;
		public static const SLENDER:int = 2;
		public static const LARGE:int = 3
		
	}

}