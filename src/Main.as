package 
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import worlds.MenuWorld;
	
	[SWF(width="1080", height="720")]
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class Main extends Engine 
	{		
		public function Main():void 
		{
			super(C.WIDTH/C.SCALE, C.HEIGHT/C.SCALE);
			FP.screen.scale = C.SCALE;
		}
		
		override public function init():void
		{
			super.init();
			
			FP.world = new MenuWorld(0);
			FP.console.enable();
			FP.volume = 0.0;
		}
	}	
}