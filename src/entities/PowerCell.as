package entities 
{
	import net.flashpunk.graphics.Image;
	/**
	 * Power cell which the player must move to a PowerCellSocket with the Tether
	 * @author Adam Docherty
	 */
	public class PowerCell extends TetherableBase
	{
		protected var _image:Image;
		
		public function PowerCell(x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			
			_image = Image.createCircle(8, 0xFFFF00);
			setHitbox(16, 16);
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			super.update();
			if (isTethered())
			{	
				checkCollisionsPowerCellSocket();
			}
		}
		
		protected function checkCollisionsPowerCellSocket():void
		{
			var collider:PowerCellSocket;
			
			collider = PowerCellSocket(collideTypes("power_cell_socket", x, y));
			
			if (collider) {
				collider.onActivate();
				world.remove(this);
			}
		}
		
	}

}