package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import worlds.GameWorld;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Switch extends Entity 
	{
		protected var _label:String;
		
		protected var _image:Image;
		
		public function Switch(x:int, y:int, label:String) 
		{
			this.x = x;
			this.y = y;
			_label = label;
			
			_image = Image.createRect(16, 16, 0x0000FF);
			
			setHitbox(16, 16);
			
			type = "switch";
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		public function onActivate():void
		{
			GameWorld(world).levelManager.switchActivated(_label);
		}
		
	}

}