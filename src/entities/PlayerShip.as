package entities 
{

	import flash.automation.Configuration;
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Key;
	import worlds.GameWorld;
	/**
	 * Class for the player controlled ship. Handles most of the collision
	 * between the ship and other entities
	 * @author Adam Docherty
	 */
	public class PlayerShip extends Entity 
	{
		protected static const SHOOT_DELAY:Number = 0.3;
		
		protected static const TELEPORT_TRAIL_SIZE:uint = 16;
		
		protected static const MAX_HEALTH:uint = 3;
		
		protected static var s_laserSfx:Sfx;
		protected static var s_teleportSfx:Sfx;
		
		protected var _image:Image;
		protected var _teleportTrail:Emitter;
		protected var _tetherBeam:Emitter;
		
		public function get teleportTrail():Emitter { return _teleportTrail; }
		public function get tetherBeam():Emitter { return _tetherBeam; }
		
		protected var _vx:Number, _vy:Number;
		protected var _speed:int;
		
		protected var _shootTimer:Number;
		
		protected var _tetheredObject:TetherableBase;
		protected var _tetherActive:Boolean;
		
		protected var _health:int;
		protected var _isDead:Boolean;
		protected var _hasBeenShot:Boolean;
		
		public function get health():int { return _health; }
		public function set health(health:int):void { _health = health; }
		
		public function get tetheredObject():TetherableBase { return _tetheredObject; }
		
		/**
		 * 
		 * @param	x	x-position of the ship's spawn
		 * @param	y	y-position of the ship's spawn
		 */
		public function PlayerShip(x:int, y:int) 
		{
			s_laserSfx = new Sfx(A.SFX_LASER);
			s_teleportSfx = new Sfx(A.SFX_TELEPORT);
			
			_image = new Image(A.IMAGE_PLAYER_SHIP);
			_image.scaledHeight = 16;
			_image.scaledWidth = 16;
			_image.centerOrigin();
			
			_vx = 0, _vy = 0;
			_speed = 150;
			
			_shootTimer = 0;
			
			// Hitbox slightly smaller than the sprite size to make fitting
			// into tight gaps easier
			setHitbox(12, 12, 6, 6);
			
			type = "player";
			
			this.x = x;
			this.y = y;
			
			graphic = _image;
			
			_teleportTrail = new Emitter(A.IMAGE_TELEPORT_TRAIL, 16, 16);
			_teleportTrail.newType("trail", [0, 1, 2, 3]);
			_teleportTrail.setAlpha("trail", 1.0, 0);
			
			_tetherBeam = new Emitter(A.IMAGE_TELEPORT_TRAIL, 4, 4);
			_tetherBeam.newType("beam", [0, 1, 2, 3]);
			_tetherBeam.setAlpha("beam", 1.0, 0.5);
			
			_health = MAX_HEALTH;
			_isDead = false;
			_hasBeenShot = false;
		}
		
		override public function update():void 
		{
			move();
			fireTether();
			
			if (Input.pressed(Key.T))
				swapPosition();
			
			//shoot();
			checkCollisions();
			
			super.update();
		}
		
		protected function move():void
		{
			var xInput:int = 0, yInput:int = 0;
			
			//// if the player is using the teleporter
			//if (!Input.check(Key.A))
			//{	
				// move the player
				if (Input.check(Key.A)) {
					--xInput;
				}
				if (Input.check(Key.D)) {
					++xInput;
				}
				if (Input.check(Key.W)) {
					--yInput;
				}
				if (Input.check(Key.S)) {
					++yInput;
				}
			//}	
			
			_vx = _speed * FP.elapsed * xInput;
			_vy = _speed * FP.elapsed * yInput;
			
			if (_vx != 0 || _vy != 0)
				_image.angle = FP.angle(0, 0, _vx, _vy) - 90;
			
			x += _vx;
			y += _vy;
		}
		
		protected function fireTether():void
		{
			if (Input.mousePressed)
			{
				if (!_tetherActive) 
				{
					_tetherActive = true;
					var angle:Number = Math.atan2((Input.mouseY - (y - FP.camera.y)), 
						(Input.mouseX - (x - FP.camera.x)));
					var tether:Tether = new Tether(this, angle);
					world.add(tether);
				} else {
					if (_tetheredObject != null)
					{
						_tetheredObject.untether();
						deactivateTether();
					}
				}
			}
			
			if (_tetheredObject != null)
			{
				var particleAngle:int = 180 + FP.angle(_tetheredObject.x, _tetheredObject.y, 
					this.x, this.y);
				var distance:int = FP.distance(_tetheredObject.x, _tetheredObject.y, x, y) ;

				_tetherBeam.setMotion("beam", particleAngle, distance, 0.2, 0, 0, 0, null);
				_tetherBeam.emit("beam", x + halfWidth, y + halfHeight);
			}
				
		}
		
		protected function swapPosition():void
		{
			if (!_tetherActive) return;
			if (collideTypes(AntiTeleportField.TYPE, x, y)) return;
			
			if (_tetheredObject == null) return;
			if (_tetheredObject.collideTypes(AntiTeleportField.TYPE, 
				_tetheredObject.x, _tetheredObject.y)) return;
			
			var newX:int, newY:int;
			
			newX = _tetheredObject.x;
			newY = _tetheredObject.y;
			
			_tetheredObject.x = x;
			_tetheredObject.y = y;
			
			teleportTo(newX, newY);
		}
		
		protected function shoot():void
		{
			// only shoots if the shoot timer has expired
			if (_shootTimer > 0) {
				_shootTimer -= FP.elapsed;
				return;
			}
			
			if (Input.check(Key.Z)) {
				s_laserSfx.play();
				var bullet:Bullet = Bullet(world.create(Bullet));
				bullet.init();
				bullet.spawn(x, y);
				
				_shootTimer = SHOOT_DELAY;
			}
		}
		
		public function teleportTo(x:int, y:int):void
		{
			// create the particle effect
			var angle:int = 180 + FP.angle(x, y, this.x, this.y);
			var distance:int = FP.distance(this.x, this.y, x, y) * 4;
			for (var i:Number = 0.0; i <= 1; i += 0.05) {
				_teleportTrail.setMotion("trail", angle, distance, i, 0, 10, 1, null);
				_teleportTrail.emit("trail", this.x, this.y);
			}
			
			// move the player
			this.x = x;
			this.y = y;
			s_teleportSfx.play();
		}
		
		/**
		 * Resolves the collision between the player and the wall
		 * @param	wall	Wall that the player collided with
		 */
		public function checkCollisionsWall():void
		{
			var wall:Entity = collideTypes("wall", x, y);
			if (!wall) wall = collideTypes(Door.DOOR_CLOSED_TYPE, x, y);			
			
			if (wall)
			{
				var dx:Number = FP.sign(_vx);
				var dy:Number = FP.sign(_vy);
				
				// TODO: Optimize collideWithWall function.
				
				// move the player back to it's previous x position and recheck 
				// for collision
				x -= _vx;
				if (!wall.collideWith(this, wall.x, wall.y)) {
					// if the player is no longer colliding with the wall, move 1
					// step back towards the wall until there is a collision
					do  {
						x += dx;
					} while (!wall.collideWith(this, wall.x, wall.y));
					// move to the previous (non colliding) position
					x -= dx;
					return;
				}
				
				// move player back to it's previous y position, but new x 
				// position, and recheck for collisions
				x += _vx;
				y -= _vy;
				
				if (!wall.collideWith(this, wall.x, wall.y)) {
					// if the player is no longer colliding with the wall, move 1
					// step back towards the wall until there is a collision
					do  {
						y += dy;
					} while (!wall.collideWith(this, wall.x, wall.y));
					// move to the previous (non colliding) position
					y -= dy;
					return;
				}
				
				// move the player back to their position in the previous frame
				x -= _vx;
				do  {
					// take 1 step towards the wall in the x axis until there is a collision
					x += dx;
				} while (!wall.collideWith(this, wall.x, wall.y));
				x -= dx;
					
				do  {
					// take 1 step towards the wall in the y axis until there is a collision
					y += dy;
				} while (!wall.collideWith(this, wall.x, wall.y));
				y -= dy;
			}
		}
		
		
		protected function isTetherActive():Boolean 
		{ 
			return _tetherActive; 
		}
		
		public function tetherTo(object:TetherableBase):void
		{
			_tetheredObject = object;
		}
		
		public function activateTether():void
		{
			_tetherActive = true;
		}
		
		public function deactivateTether():void
		{
			_tetherActive = false;
			_tetheredObject = null;
		}
		public function checkIsDead():Boolean
		{ 
			return _isDead; 
		}
		protected function checkCollisions():void
		{
			// handles all colisions between the player and other small entities
			checkCollisionsPod();
			checkCollisionsSwitch();
			checkCollisionsWall();
			checkCollisionsEnemy();
		}
		public function collidedWithBullet(damage:int):void
		{
			_hasBeenShot = true;
			
			_health -= damage;
			
			if (_health <= 0)
				_isDead = true;
		}
		protected function checkCollisionsPod():void
		{
			var collider:Pod;
			
			collider = Pod(collideTypes("pod", x, y));
			
			if (collider) {
				world.remove(collider);
			}
		}
		protected function checkCollisionsEnemy():void
		{
			var collider:EnemySentry;
			
			collider = EnemySentry(collideTypes("enemy", x, y));
			
			if (collider) {
				_isDead = true;
			}
		}
		
		protected function checkCollisionsEnemyBullet():void
		{
			var collider:EnemySentry;
			
			collider = EnemySentry(collideTypes("enemy_bullet", x, y));
			
			if (collider) {
				_isDead = true;
			}
		}
		
		protected function checkCollisionsSwitch():void
		{
			var collider:Switch;
			
			collider = Switch(collideTypes("switch", x, y));
			
			if (collider) {
				collider.onActivate();
			}
		}
		
		/*protected function checkCollisionsDoor():void
		{
			var collider:Door;
			
			collider = Door(collideTypes(Door.DOOR_CLOSED_TYPE, x, y));
			
			if (collider) {
				if (collider.collideWith(this, x, y)) {
					collideWithWall(collider);
				}
			}
		}*/
	}
}