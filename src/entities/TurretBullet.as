package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import worlds.GameWorld;
	/**
	 * Bullet fired by a turret.
	 * @author Michael Cameron 18/11/2013
	 */
	public class TurretBullet extends Entity 
	{
		protected static const EXPIRE_TIME:Number = 3;
		
		protected var _image:Image;
		
		protected var _speed:int;
		protected var _vx:int;
		protected var _vy:int;
		
		protected var _expires:Number;

		protected var _enemy:EnemySentry;
		protected var _isHoming:Boolean;
		
		public function TurretBullet() 
		{
			_image = new Image(A.IMAGE_PLAYER_BULLET);
			_image.color = 0x00FFFF;
			_speed = 250;
			_vx = 0;
			_vy = 0;
			
			type = "turret_bullet"
			setHitbox(4, 8, 0, 0);
			_isHoming = true;
			init();
		}
		public function init():void
		{
			_expires = 3;
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			if (_enemy == null)
			{
				world.remove(this);
				return;
			}
			
			move();
			
			super.update();
			
			if (_isHoming) {
				calculateVelocity();
			}
			
			if (isExpired()) {
				world.recycle(this);
			}
			
			checkCollisionWall();
			
			checkCollisionEnemy();
		}
		
		/**
		 * Must be called after the bullet is created so it can be 
		 * reinitialised after recycling
		 * @param	x	x-position of the bullet's spawn
		 * @param	y	y-position of the bullet's spawn
		 */
		public function spawn(x:int, y:int, enemy:EnemySentry ):void
		{
			this.x = x;
			this.y = y;
			
			_enemy = enemy;
	
			init();
			
			calculateVelocity();
		}
		/**
		 * calculateVelocity()
		 * Calculates the new velocity of the bullet towards the player.
		 */
		protected function calculateVelocity():void
		{
			if (!_enemy._hasBeenShot)
			{
				var deltaX:Number = this.x - _enemy.x;
				var deltaY:Number = this.y - _enemy.y;

				var magnitude:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
				var velocityScale:Number = _speed / magnitude;
				_image.angle = (Math.atan2(y - _enemy.y, x -_enemy.x)) * -(180 / Math.PI) + 90;
				_vx = deltaX * velocityScale;
				_vy = deltaY * velocityScale;
			}
		}
		protected function move():void
		{
			x -= _vx * FP.elapsed;
			y -= _vy * FP.elapsed;
		}
		
		protected function isExpired():Boolean
		{
			_expires -= FP.elapsed;
			if (_expires <= 0)
			{
				return true;
			}
			if (_vx == 0 && _vy == 0)
			{
				return true;
			}
			return false;
		}
		protected function checkCollisionWall():void
		{
			if (GameWorld(world).map.collideWith(this, 0, 0))
			{
				world.remove(this);
			}
		}
		protected function checkCollisionEnemy():void
		{
			var collider:EnemySentry;
			collider = EnemySentry(this.collideTypes("enemy", this.x, this.y));
			if (collider)
			{
				collider.collidedWithBullet();
				world.remove(this);
			}
		}
		
		
	}

}