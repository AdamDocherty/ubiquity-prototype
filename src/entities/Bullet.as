package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	/**
	 * Bullet fired by the player.
	 * @author Adam Docherty
	 */
	public class Bullet extends Entity 
	{
		protected static const EXPIRE_TIME:Number = 3;
		
		protected var _image:Image;
		
		protected var _speed:int;
		protected var _expires:Number;
		
		public function Bullet() 
		{
			_image = new Image(A.IMAGE_PLAYER_BULLET);
			_speed = 250;
			
			type = "player"
			setHitbox(4, 8, 0, 0);
			
			init();
		}
		
		public function init():void
		{
			_expires = 3;
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			move();
			
			super.update();
			
			if (isExpired())
			{
				world.recycle(this);
			}
		}
		
		/**
		 * Must be called after the bullet is created so it can be 
		 * reinitialised after recycling
		 * @param	x	x-position of the bullet's spawn
		 * @param	y	y-position of the bullet's spawn
		 */
		public function spawn(x:int, y:int):void
		{
			this.x = x;
			this.y = y;
			
			init();
		}
		
		protected function move():void
		{
			y -= _speed * FP.elapsed;
		}
		
		protected function isExpired():Boolean
		{
			_expires -= FP.elapsed;
			if (_expires <= 0)
			{
				return true;
			}
			return false;
		}
		
	}

}