package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	
	/**
	 * ...
	 * @author ...
	 */
	public class AntiTeleportField extends Entity 
	{
		public static const TYPE:String = "no-teleport"
		
		protected var _image:Image;
		

		public function AntiTeleportField(x:Number, y:Number, width:Number, height:Number) 
		{
			_image = Image.createRect(width, height, 0xFF80FF, 1);
			setHitbox(width, height);

			type = TYPE;

			super(x, y, _image);
		}
		
	}

}