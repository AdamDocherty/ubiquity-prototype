package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.TiledImage;
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class FinishArea extends Entity 
	{
		protected var _image:TiledImage;
		

		public function FinishArea(x:Number, y:Number, width:Number, height:Number) 
		{
			_image = new TiledImage(A.IMAGE_FINISH, width, height);
			setHitbox(width, height);

			type = "finish";

			super(x, y, _image);
		}

	}

}