package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import worlds.GameWorld;
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class PowerCellSocket extends Entity 
	{
		protected var _poweredDownImage:Image;
		protected var _poweredUpImage:Image;
		
		public function PowerCellSocket(x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			
			_poweredDownImage = Image.createRect(16, 16, 0x808080);
			_poweredUpImage = Image.createRect(16, 16, 0xFFFF00);
			
			setHitbox(16, 16);
			type = "power_cell_socket";
		}
		
		override public function added():void 
		{
			graphic = _poweredDownImage;
			
			super.added();
		}
		
		public function onActivate():void
		{
			graphic = _poweredUpImage;
			GameWorld(world).levelManager._numberPowerCellSockets--;
			trace(GameWorld(world).levelManager._numberPowerCellSockets);
		}
		
	}

}