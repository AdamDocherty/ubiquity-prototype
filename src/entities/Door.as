package entities 
{
	import flash.display.Graphics;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Door extends Entity 
	{
		public static const DOOR_CLOSED_TYPE:String = "door-closed";
		public static const DOOR_OPEN_TYPE:String = "door-open";
		
		protected var _label:String;
		protected var _opened:Boolean;
		
		protected var _image:Image;
		
		public function get label():String { return _label; }
		
		public function Door(x:int, y:int, label:String) 
		{
			this.x = x;
			this.y = y;
			_label = label;
			_opened = false;
			
			_image = Image.createRect(16, 16, 0xFF0000);
			
			setHitbox(16, 16);
			
			type = DOOR_CLOSED_TYPE;
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		public function open():void
		{
			_opened = true;
			visible = false;
			type = DOOR_OPEN_TYPE;
		}
		
		public function close():void
		{
			_opened = false;
			visible = true;
			type = DOOR_CLOSED_TYPE;
		}
		
	}

}