package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import worlds.GameWorld;
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class TeleportCursor extends Entity 
	{
		private var _image:Image;
		
		private var _player:PlayerShip;
		
		private var _speed:int;
		private var _valid:Boolean;
		
		public function TeleportCursor(player:PlayerShip)
		{
			_player = player;
			_image = new Image(A.IMAGE_TELEPORT_CURSOR);
			
			_speed = 250;
			
			setHitbox(20, 20, 2, 2);
		}
		
		override public function added():void 
		{			
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			
			if (Input.released(Key.A))
			{
				if (isValid())
				{
					_player.teleportTo(x, y);
				}
			}
			
			if (Input.check(Key.A))
			{
				visible = true;
				move();
				if (isValid())
				{
					// sets cursor colour to red
					_image.color = 0xFFFFFF;
				} else 
				{
					_image.color = 0xFF0000;
				}
			} else {				
				x = _player.x;
				y = _player.y;
				
				visible = false;
			}
			
			_valid = true;
			
			super.update();
		}
		
		private function move():void
		{			
			var xInput:int = 0, yInput:int = 0;
			
			// move the player
			if (Input.check(Key.LEFT)) {
				--xInput;
			}
			if (Input.check(Key.RIGHT)) {
				++xInput;
			}
			if (Input.check(Key.UP)) {
				--yInput;
			}
			if (Input.check(Key.DOWN)) {
				++yInput;
			}
			
			x += _speed * FP.elapsed * xInput;
			y += (_speed * yInput - GameWorld.s_scrollSpeed) * FP.elapsed;
			
			keepOnScreen();
		}
		
		private function keepOnScreen():void
		{
			var screenX:int, screenY:int;
			
			screenX = left - FP.camera.x;
			screenY = top - FP.camera.y;
			
			if (screenX < 0) x = FP.camera.x + (x - left);
			if (screenY < 0) y = FP.camera.y + (y - top);
			if (screenX > FP.screen.width - width) x = FP.camera.x + FP.screen.width - width;
			if (screenY > FP.screen.height - height) y = FP.camera.y + FP.screen.height - height;
		}
		
		private function isValid():Boolean
		{
			// invalid if on top of player (reduces spammy sound effects)
			if (collideWith(_player, x, y)) {
				_valid = false;
			}
			return _valid;
		}
		
		public function collideWithWall():void
		{
			_valid = false;
		}
		
	}

}