package entities 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author ...
	 */
	public class Path 
	{		
		private var _path:Vector.<Node>;
		
		public function get length():uint { return _path.length; }
		
		public function Path() 
		{
			_path = new Vector.<Node>;
		}
		
		public function addNode(x:int, y:int):void
		{
			_path.push(new Node(x, y));
		}
		
		public function getPositionAtIndex(index:uint, x:int, y:int):Point
		{
			if (index > length) {
				trace("Trying to access index " + index + " which is outside the range of the path. Length = " + length);
				return null;
			}
			
			x = _path[index].x;
			y = _path[index].y;
			
			return new Point(x, y);
		}
		
	}

}

class Node
{
	private var _x:int;
	private var _y:int;
	
	private var _prev:Node;
	private var _next:Node;
	
	public function get x():int { return _x; }
	public function get y():int { return _y; }
	
	public function get prev():Node { return _prev; }
	public function get next():Node { return _next; }
	
	public function set prev(node:Node):void { prev = node; }
	public function set next(node:Node):void { next = node; }
	
	public function Node(x:int, y:int, prev:Node = null)
	{
		_x = x;
		_y = y;
		
		if (prev != null) {
			_prev = prev;
			prev.next = this;
		}
	}
}