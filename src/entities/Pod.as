package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	
	/**
	 * Pod that the player can pick up for extra points
	 * @author Adam Docherty
	 */
	public class Pod extends Entity 
	{

		protected var _image:Image;
		
		/**
		 * 
		 * @param	x	x-position of the pod's spawn
		 * @param	y	y-position of the pod's spawn
		 */
		public function Pod(x:int, y:int) 
		{
			_image = new Image(A.IMAGE_POD);
		
			this.x = x + 2;
			this.y = y;
			
			type = "pod";
			setHitbox(28, 16, 0, 0);

		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
	}

}