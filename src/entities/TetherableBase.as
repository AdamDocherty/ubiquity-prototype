package entities 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	
	/**
	 * Base class for all objects that can be moved by the tether.
	 * @author Adam Docherty
	 */
	public class TetherableBase extends Entity 
	{
		/** The ship that the object is tethered to, if NULL then the object is not tethered */
		protected var _player : PlayerShip;
		protected var _xOffset : int;
		protected var _yOffset : int;
		
		public function isTetherable() : Boolean
		{
			return true;
		}
		
		public function isTethered() : Boolean { return (null != _player); }
		
		public function tetherTo(player:PlayerShip):void
		{
			_player = player;
			_player.tetherTo(this);
			_xOffset = x - player.x;
			_yOffset = y - player.y;
		}
		
		public function untether():void
		{
			_player = null;
		}
		
		public function TetherableBase() 
		{
			_player = null;
			type = "tetherable";
		}
		
		override public function update():void 
		{
			super.update();
			if (isTethered())
			{
				//move();
			}
		}
		
		override public function removed():void 
		{
			super.removed();
			if (isTethered())
			{
				_player.deactivateTether();
			}
		}
		
		public function move():void 
		{
			var p:Point = new Point;
			
			var wall:Entity;
			wall = world.collideLine("wall", _player.x + _player.halfWidth, _player.y + _player.halfHeight, 
			Input.mouseX + FP.camera.x, Input.mouseY + FP.camera.y, 16, p);
			if (!wall) wall = world.collideLine(Door.DOOR_CLOSED_TYPE, _player.x + _player.halfWidth, _player.y + _player.halfHeight, 
			Input.mouseX + FP.camera.x, Input.mouseY + FP.camera.y, 16, p);
			
			x = p.x - halfWidth;
			y = p.y - halfHeight;
		}
		
	}

}