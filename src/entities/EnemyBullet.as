package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import worlds.GameWorld;
	
	/**
	 * Bullet fired by an enemy
	 * @author Michael Cameron 18/11/2013
	 */
	public class EnemyBullet extends Entity 
	{
		protected static const EXPIRE_TIME:Number = 3;
		
		protected var _image:Image;
		
		protected var _speed:int;
		protected var _vx:int;
		protected var _vy:int;
		protected var _damage:int;
		
		protected var _expires:Number;

		protected var _player:PlayerShip;
		protected var _isHoming:Boolean;
		
		public function EnemyBullet() 
		{
			_image = new Image(A.IMAGE_PLAYER_BULLET);
			centerOrigin();
			_speed = 250;
			_vx = 0;
			_vy = 0;			
			
			type = "enemy_bullet"
			setHitbox(2, 2, 2, 2);
			_isHoming = true;
			init();
		}
		
		public function init():void
		{
			_expires = 3;
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			move();
			
			super.update();
			
			if (_isHoming) {
				calculateVelocity();
			}
			
			if (isExpired()) {
				world.recycle(this);
			}
			
			checkCollisionWall();
			checkCollisionPlayer();
		}
		
		/**
		 * Must be called after the bullet is created so it can be 
		 * reinitialised after recycling
		 * @param	x	x-position of the bullet's spawn
		 * @param	y	y-position of the bullet's spawn
		 */
		public function spawn(x:int, y:int, player:PlayerShip, damage:int = 1):void
		{
			this.x = x;
			this.y = y;
			
			_player = player;
			_damage = damage;
			
			switch(_damage) {
				case 1: _image.color = 0x00FFFF; break;
				case 2: _image.color = 0xFFFF00; break;
				case 3: _image.color = 0xFF0000; break;
				default: _image.color = 0xFFFFFF;
			}
	
			init();
			
			calculateVelocity();
		}
		/**
		 * calculateVelocity()
		 * Calculates the new velocity of the bullet towards the player.
		 */
		protected function calculateVelocity():void
		{
			var deltaX:Number = this.x - (_player.x + _player.halfWidth);
			var deltaY:Number = this.y - (_player.y + _player.halfHeight);

			var magnitude:Number = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			var velocityScale:Number = _speed / magnitude;
			_image.angle = (Math.atan2(y - _player.y, x -_player.x)) * -(180 / Math.PI) + 90;
			_vx = deltaX * velocityScale;
			_vy = deltaY * velocityScale;
		}
		protected function move():void
		{
			x -= _vx * FP.elapsed;
			y -= _vy * FP.elapsed;
		}
		
		protected function isExpired():Boolean
		{
			_expires -= FP.elapsed;
			if (_expires <= 0)
			{
				return true;
			}
			return false;
		}
		protected function checkCollisionWall():void
		{
			if (GameWorld(world).map.collideWith(this, 0, 0))
			{
				world.remove(this);
			}
		}
		protected function checkCollisionPlayer():void
		{
			if (this.collideWith(_player, this.x, this.y))
			{
				_player.collidedWithBullet(_damage);
				world.remove(this);
			}
		}
		
	}

}