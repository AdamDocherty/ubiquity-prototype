package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import worlds.GameWorld;
	
	/**
	 * A tether which the player fires from her ship. Can attach to tetherable objects.
	 * @author Adam Docherty
	 */
	public class Tether extends Entity 
	{
		protected static const TETHER_SPEED:Number = 600.0;
		
		protected var _image:Image;
		protected var _playerShip:PlayerShip;
		
		protected var _vx:Number;
		protected var _vy:Number;
		
		/**
		 * @param	playerShip	The ship that fired the tether.
		 * @param	angle	The angle the ship fired the tether at in radians.
		 */
		public function Tether(playerShip:PlayerShip, angleRad:Number) 
		{
			_playerShip = playerShip;
			_playerShip.activateTether();
			
			_image = Image.createCircle(4, 0x8080FF);
			
			x = playerShip.x;
			y = playerShip.y;
			
			// TODO : Calculate velocity based on the angle
			_vx = TETHER_SPEED * Math.cos(angleRad);
			_vy = TETHER_SPEED * Math.sin(angleRad);
			
			setHitbox(8, 8, 0, 0);
		}
		
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		
		override public function update():void 
		{
			move();
			checkCollisionWall();
			checkCollisionTetherable();
			
			super.update();
		}
		
		protected function move():void
		{
			x += _vx * FP.elapsed;
			y += _vy * FP.elapsed;
		}
		
		protected function checkCollisionWall():void
		{
			if (GameWorld(world).map.collideWith(this, 0, 0))
			{
				_playerShip.deactivateTether();
				world.remove(this);
			}
		}
		
		protected function checkCollisionTetherable():void
		{
			var collider:TetherableBase;
			
			collider = TetherableBase(collideTypes("tetherable", x, y));
			
			if (collider) {
				collider.tetherTo(_playerShip);
				world.remove(this);
			}
		}
		
	}

}