package entities.ui 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import utils.LevelManager;
	import worlds.GameWorld;
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class PowerCellsCollectedText extends Entity 
	{
		private static const UI_POSITION_X:int = 20;
		private static const UI_POSITION_Y:int = 40;
		
		private var _totalSockets:int;
		private var _currentSockets:int;
		private var _text:Text;
		
		public function PowerCellsCollectedText(sockets:int) 
		{
			_currentSockets = 0;
			_totalSockets = sockets;
			_text = new Text("Sockets:", 0, 0, { font:"orbitron black", size:16, color:0x800000 } );
		}
		
		override public function added():void 
		{
			graphic = _text;
			
			super.added();
		}
		
		override public function update():void 
		{
			_currentSockets = - GameWorld(world).levelManager._numberPowerCellSockets + _totalSockets;
			_text.text = "Sockets: " + _currentSockets + "/" + _totalSockets;
			switch(_currentSockets) {
				case 0: _text.color = 0x800000; break;
				case _totalSockets: _text.color = 0x008000; break;
				default: _text.color = 0x808000;
			}
			
			super.update();
		}
		
		override public function render():void 
		{
			x = FP.camera.x + UI_POSITION_X;
			y = FP.camera.y + UI_POSITION_Y;
			
			super.render();
		}
		
	}

}

