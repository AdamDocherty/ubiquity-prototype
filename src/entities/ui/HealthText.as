package entities.ui 
{
	import entities.PlayerShip;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author Adam Docherty
	 */
	public class HealthText extends Entity 
	{
		private static const UI_POSITION_X:int = 20;
		private static const UI_POSITION_Y:int = 20;
		
		private var _player:PlayerShip;
		private var _text:Text;
		
		public function HealthText(player:PlayerShip) 
		{
			_player = player;
			_text = new Text("Health:", 0, 0, { font:"orbitron black", size:16, color:0x008080 } );
		}
		
		override public function added():void 
		{
			graphic = _text;
			
			super.added();
		}
		
		override public function update():void 
		{
			_text.text = "Health: " + _player.health;
			
			super.update();
		}
		
		override public function render():void 
		{
			x = FP.camera.x + UI_POSITION_X;
			y = FP.camera.y + UI_POSITION_Y;
			
			super.render();
		}
		
	}

}