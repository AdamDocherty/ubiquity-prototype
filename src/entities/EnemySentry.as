package entities 
{
	import enums.EnemyType;
	import enums.PatrolState;
	import flash.display.GraphicsSolidFill;
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	import net.flashpunk.FP;
	import worlds.GameWorld;
	/**
	 * Enemy tyoe that has a kill box attached to it, destroying the player if they move within its range.
	 * Enemy will fire at player once they are sighted.
	 * @author Michael Cameron 17/11/2013
	 */
	public class EnemySentry extends Entity 
	{		
		protected static const AGRO_DISTANCE:Number = 100;
		protected static const AGRO_ARC_ANGLE:Number = 90;
		protected static const SHOOT_DELAY:Number = 0.2;
		
		protected static var s_laserSfx:Sfx;
		
		protected var _image:Image;
		protected var _player:PlayerShip;
		protected var _playerInRange:Boolean;
		protected var _shootTimer:Number;
		protected var _angleDegrees:Number;		
		public var _hasBeenShot:Boolean;
		
		protected var _speed:int;
		protected var _damage:int;
		
		protected var _path:Path;
		protected var _patrolState:int;
		protected var _patrolIndex:int;
		
		public function set player(player:PlayerShip):void { _player = player; _playerInRange = false; }
		
		public function EnemySentry(x:int, y:int, angleDegrees:Number, player:PlayerShip, enemyType:int = 1, path:Path = null)
		{
			this.x = x;
			this.y = y;
			
			type = "enemy";
			
			s_laserSfx = new Sfx(A.SFX_LASER);
			
			_angleDegrees = angleDegrees;
			
			_player = player;
			
			_playerInRange = false;
			_shootTimer = 0;
			_hasBeenShot = false;
			
			switch(enemyType) {
				case EnemyType.SMALL:
					_speed = 50;
					_damage = 1;
					_image = new Image(A.IMAGE_ALIEN);
					setHitbox(16, 16, 8, 8);
					break;
				case EnemyType.SLENDER:
					_speed = 100;
					_damage = 2;
					_image = new Image(A.IMAGE_SLENDER_ALIEN);
					_image.scaledWidth = 24;
					_image.scaledHeight = 32;
					setHitbox(24, 24, 12, 12);
					break;
				case EnemyType.LARGE:
					_speed = 0;
					_damage = 3;
					_image = new Image(A.IMAGE_LARGE_ALIEN);
					_image.scaledWidth = 32;
					_image.scaledHeight = 32;
					setHitbox(32, 32, 16, 16);
					break;
			}
			
			_image.centerOrigin();
			_image.angle = 360 - _angleDegrees;
			
			_path = path;
			_patrolState = PatrolState.FORWARD;
			_patrolIndex = 1;
		}
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		override public function update():void 
		{
			move();
			searchForPlayer();
			if (_playerInRange) {
				shoot();
			}
			super.update();
			
			if (_hasBeenShot)
			{
				world.remove(this);
			}
		}
		
		protected function move():void
		{
			if (_path == null) return; 
			
			var skipNode:Boolean = false;
			
			var toX:int, toY:int;
			
			var p:Point;
			
			p = _path.getPositionAtIndex(_patrolIndex, toX, toY);
			toX = p.x;
			toY = p.y;
			
			var angle:Number = FP.angle(x, y, toX, toY)
			
			_angleDegrees = - angle + 90;
			_image.angle = 360 - _angleDegrees;
			
			FP.angleXY(p, angle, 1);
		
			x += p.x * _speed * FP.elapsed;
			y += p.y * _speed * FP.elapsed;
			
			if (collideTypes(Door.DOOR_CLOSED_TYPE, x, y)) {
				x -= p.x * _speed * FP.elapsed;
				y -= p.y * _speed * FP.elapsed;
				skipNode = true;
			}
			
			if (skipNode || collidePoint(x, y, toX, toY))
			{
				if (_patrolState == PatrolState.FORWARD) {
					if (_patrolIndex + 1 >= _path.length) {
						_patrolIndex--;
						_patrolState = PatrolState.REVERSE;
					} else {
						_patrolIndex++;
					}
				} else if (_patrolState == PatrolState.REVERSE) {
					if (_patrolIndex - 1 < 0) {
						_patrolIndex++;
						_patrolState = PatrolState.FORWARD;
					} else {
						_patrolIndex--;
					}
				}
			}
		}
		
		protected function searchForPlayer():void
		{			
			var distance:Number = Math.sqrt((this.x - _player.x) * (this.x - _player.x) + (this.y - _player.y) * (this.y - _player.y));
			if (distance <= AGRO_DISTANCE) {
				//trace("in range ", distance);
				var deltaX:Number = (this.x) - (_player.x + _player.halfWidth);
				var deltaY:Number = (this.y) - (_player.y + _player.halfHeight);
				var deltaAngle:Number = (Math.atan2(deltaY, deltaX)) * (180 / Math.PI);
				deltaAngle += 270;
				deltaAngle -= _angleDegrees;
				if (deltaAngle > 270) {
					deltaAngle -= 360;
				}
				if ((deltaAngle < AGRO_ARC_ANGLE) && (deltaAngle > -AGRO_ARC_ANGLE)) {
					_playerInRange = true;
				} else {
					_playerInRange = false;
				}
			} else {
				//trace("out of range ", distance);
				_playerInRange = false;
				return;
			}
			
			var wall:Entity;
			wall = world.collideLine("wall", x, y, _player.x, _player.y);
			
			if (wall != null)
			{
				_playerInRange = false;
				return;
			}
		}
		protected function shoot():void
		{
			// only shoots if the shoot timer has expired
			if (_shootTimer > 0) {
				_shootTimer -= FP.elapsed;
				return;
			}
			
			//if (Input.check(Key.Z)) {
				s_laserSfx.play();
				var bullet:EnemyBullet = EnemyBullet(world.create(EnemyBullet));
				bullet.init();
				bullet.spawn(this.x, this.y, _player, _damage);
				
				_shootTimer = SHOOT_DELAY;
			//}
		}
		public function collidedWithBullet():void
		{
			_hasBeenShot = true;
		}
		public function onCollideWithTrap():void
		{
			// Send the enemy into the other dimesnion.
			GameWorld(world).levelManager.swapObject(this);
		}
	}
	
}