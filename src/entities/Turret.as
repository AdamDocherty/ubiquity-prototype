package entities 
{
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	import net.flashpunk.FP;
	/**
	 * A Turret which will fire at the closest enemy within its range.
	 * @author Michael Cameron 18/11/2013
	 */
	public class Turret extends TetherableBase 
	{
		protected static const AGRO_DISTANCE:Number = 100;
		protected static const AGRO_ARC_ANGLE:Number = 360;
		protected static const SHOOT_DELAY:Number = 0.1;
		
		protected static var s_laserSfx:Sfx;
		
		protected var _image:Image;
		protected var _enemyInRange:Boolean;
		protected var _shootTimer:Number;
		protected var _angleDegrees:Number;
		protected var _currentTarget:EnemySentry;
		public function Turret(x:int, y:int, angleDegrees:Number) 
		{
			this.x = x;
			this.y = y;
			
			s_laserSfx = new Sfx(A.SFX_LASER);
			
			_angleDegrees = angleDegrees;
			
			_image = Image.createCircle(8, 0xFF0000);
			//_image.centerOrigin();
			_image.angle = 360 - _angleDegrees;
			
			setHitbox(16, 16);
	
			//_player = player;
			
			_enemyInRange = false;
			_shootTimer = 0;
		}
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		override public function update():void 
		{
			searchForEnemies();
			if (_enemyInRange && _currentTarget) {
				shoot();
			}
			super.update();
		}
		protected function searchForEnemies():void
		{
			var enemy:EnemySentry;
			var enemyVector:Vector.<EnemySentry> = new Vector.<EnemySentry>;
			while (enemyVector.length > 0) {
				enemyVector.pop();
			}
			_currentTarget = null;
			world.getType("enemy", enemyVector);
			
			if (enemyVector.length == 0) {
				_enemyInRange = false;
				return;
			}
			// Find the closest enemy to the turret when in range.
			var shortestDistance:Number = AGRO_DISTANCE;
			var distance:Number;
			for each (enemy in enemyVector) {
				distance = Math.sqrt((this.x - enemy.x) * (this.x - enemy.x) + (this.y - enemy.y) * (this.y - enemy.y));
				if (distance < shortestDistance) {
					_currentTarget = enemy;
					shortestDistance = distance;
					_enemyInRange = true;
				}
			}
			
			if (_currentTarget) {
				var deltaX:Number = (this.x) - (_currentTarget.x + _currentTarget.halfWidth);
				var deltaY:Number = (this.y) - (_currentTarget.y + _currentTarget.halfHeight);
				var deltaAngle:Number = (Math.atan2(deltaY, deltaX)) * (180 / Math.PI);
				deltaAngle += 270;
				deltaAngle -= _angleDegrees;
				if (deltaAngle > 270) {
					deltaAngle -= 360;
				}
				
				if ((deltaAngle < AGRO_ARC_ANGLE) && (deltaAngle > -AGRO_ARC_ANGLE)) {
					_enemyInRange = true;
				} else {
					_enemyInRange = false;
				}
				
			} else {
				_enemyInRange = false;
			}
			
			/*var distance:Number = Math.sqrt((this.x - _player.x) * (this.x - _player.x) + (this.y - _player.y) * (this.y - _player.y));
			if (distance <= AGRO_DISTANCE) {
				//trace("in range ", distance);
				var deltaX:Number = (this.x) - (_player.x + _player.halfWidth);
				var deltaY:Number = (this.y) - (_player.y + _player.halfHeight);
				var deltaAngle:Number = (Math.atan2(deltaY, deltaX)) * (180 / Math.PI);
				deltaAngle += 270;
				deltaAngle -= _angleDegrees;
				if (deltaAngle > 270) {
					deltaAngle -= 360;
				}
				if ((deltaAngle < AGRO_ARC_ANGLE) && (deltaAngle > -AGRO_ARC_ANGLE)) {
					_playerInRange = true;
				} else {
					_playerInRange = false;
				}
			} else {
				//trace("out of range ", distance);
				_playerInRange = false;
			}*/
		}
		protected function shoot():void
		{
			// only shoots if the shoot timer has expired
			if (_shootTimer > 0) {
				_shootTimer -= FP.elapsed;
				return;
			}
			
			//if (Input.check(Key.Z)) {
				s_laserSfx.play();
				var bullet:TurretBullet = TurretBullet(world.create(TurretBullet));
				bullet.init();
				bullet.spawn(this.x, this.y, _currentTarget );
				
				_shootTimer = SHOOT_DELAY;
			//}
		}
		
	}

}