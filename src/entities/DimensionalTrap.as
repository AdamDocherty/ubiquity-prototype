package entities 
{
	import net.flashpunk.graphics.Image;
	/**
	 * A tetherable trap which send enemies to the other dimension.
	 * @author Michael Cameron 16/01/2014
	 */
	public class DimensionalTrap extends TetherableBase 
	{
		protected var _image:Image;
		
		public function DimensionalTrap(x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			
			_image = new Image(A.IMAGE_DIMENSIONAL_TRAP);
			_image.centerOrigin();
			
			setHitbox(32, 32, 16, 16);
		}
		override public function added():void 
		{
			graphic = _image;
			
			super.added();
		}
		override public function update():void 
		{
			super.update();
			
			checkCollisionsEnemies();
			
		}
		protected function checkCollisionsEnemies():void
		{
			var collider:EnemySentry;
			
			collider = EnemySentry(collideTypes("enemy", x, y));
			
			if (collider) {
				collider.onCollideWithTrap();
			}
		}
	}
	

}