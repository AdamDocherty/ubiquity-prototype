package  
{
	/**
	 * Game Assets used for Velocity
	 * @author Adam Docherty
	 */
	public class A 
	{
		
		/** Images */
		//[Embed(source = "../assets/images/player_ship.png")]
		[Embed(source = "../assets/images/Ship_Sprite.png")]
		public static const IMAGE_PLAYER_SHIP:Class;
		[Embed(source = "../assets/images/player_bullet.png")]
		public static const IMAGE_PLAYER_BULLET:Class;
		[Embed(source = "../assets/images/pod.png")]
		public static const IMAGE_POD:Class;
		[Embed(source = "../assets/images/teleport_cursor.png")]
		public static const IMAGE_TELEPORT_CURSOR:Class;
		[Embed(source = "../assets/images/teleport_trail.png")]
		public static const IMAGE_TELEPORT_TRAIL:Class;
		[Embed(source = "../assets/images/finish_line.png")]
		public static const IMAGE_FINISH:Class;
		[Embed(source="../assets/images/vvv_large.png")]
		public static const IMAGE_BOSS:Class;
		[Embed(source = "../assets/images/alien.png")]
		public static const IMAGE_ALIEN:Class;
		[Embed(source = "../assets/images/Regular_Alien_01.png")]
		public static const IMAGE_SLENDER_ALIEN:Class;
		[Embed(source = "../assets/images/Fat_Alien_01.png")]
		public static const IMAGE_LARGE_ALIEN:Class;
		[Embed(source = "../assets/images/dimensional_trap.png")]
		public static const IMAGE_DIMENSIONAL_TRAP:Class;
		
		/** Fonts */
		[Embed(source = '../assets/fonts/Orbitron Black.ttf', embedAsCFF = "false", fontFamily = 'orbitron black')] 
		public static const FONT_ORBITRON_BLACK:Class;
		
		/** Levels, use format "LEVEL_xx_A/B" */
/*		[Embed(source = "../assets/levels/level_01_A.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_01_A:Class;
		[Embed(source = "../assets/levels/level_01_B.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_01_B:Class;
		[Embed(source = "../assets/levels/level_02_A.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_02_A:Class;
		[Embed(source = "../assets/levels/level_02_B.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_02_B:Class;*/
		[Embed(source = "../assets/levels/level_03_A.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_01_A:Class;
		[Embed(source="../assets/levels/level_03_B.oel", mimeType="application/octet-stream")]
		public static const LEVEL_01_B:Class;
		[Embed(source = "../assets/levels/level_04_A.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_02_A:Class;
		[Embed(source = "../assets/levels/level_04_B.oel", mimeType = "application/octet-stream")]
		public static const LEVEL_02_B:Class;
		
		/** Music */
		[Embed(source="../assets/music/dark_spooky.mp3")]
		public static const MUSIC_GAME_DEFAULT_BGM:Class;
		[Embed(source="../assets/music/intro_music.mp3")]
		public static const MUSIC_MENU_BGM:Class;
		
		/** SFX */
		[Embed(source="../assets/sfx/laser.mp3")]
		public static const SFX_LASER:Class;
		[Embed(source="../assets/sfx/teleport.mp3")]
		public static const SFX_TELEPORT:Class;
		[Embed(source="../assets/sfx/1 Doors_Switches_DimentionSwapping  Ideas-1-8.mp3")]
		public static const SFX_DOOR:Class;
		[Embed(source = "../assets/sfx/1 Doors_Switches_DimentionSwapping  Ideas-1-3.mp3")]
		public static const SFX_WORLD_SWAP:Class;
	}

}