package  
{
	/**
	 * Game constants used for Velocity
	 * @author Adam Docherty
	 */
	public class C 
	{
		
		public static const WIDTH:uint = 1080;
		public static const HEIGHT:uint = 720;
		public static const SCALE:Number = 2;
		
		public static const LEVEL_GRID_SIZE:uint = 16;
		
	}

}